#include <fstream>
#include <iostream>
#include <stack>

#include "parser.hpp"
//#include "xp_alg.hpp"

using namespace boost;
//using namespace std;

template <class Graph, class ColorMap>
void greedy_coloring(Graph &G, ColorMap colors)
{
	typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
	typedef typename boost::graph_traits<Graph>::vertex_iterator VertexI;
	typedef typename boost::graph_traits<Graph>::adjacency_iterator AdjI;
	std::stack<Vertex> uncolored;
	VertexI vi,ve;
	for(boost::tie(vi,ve) = vertices(G); vi != ve; ++vi)
	{
		Vertex v  = *vi;
		int cv = boost::get(colors, v);
		if(cv == -1)
			uncolored.push(v);
	}
	//Vertex v = *vertices(G).first;
	//uncolored.push(v);

	while(!uncolored.empty())
	{
		Vertex v = uncolored.top();
		uncolored.pop();
		int c = get(colors, v);
		if(c != -1)
			continue;
		AdjI ui, ue;
		std::set<int> blockedColors;
		for(tie(ui, ue) = adjacent_vertices(v, G); ui != ue; ui++)
		{
			Vertex u = *ui;
			int colorU = get(colors, u);
			if(colorU != -1)
				blockedColors.insert(colorU);
			else
				uncolored.push(u);
		}
		int colorV = 0;
		for(auto ci = blockedColors.begin() ; ci != blockedColors.end() ; ci++)
		{
			int c = *ci;
			if(colorV == c)
				colorV++;
		}
		boost::put(colors, v, colorV);
	}
}

typedef adjacency_list<vecS, vecS, undirectedS, property<vertex_index_t, std::size_t>> Graph;
int main(int argc, char **argv)
{
	Graph g;
	if(argc == 1)
		parsePace(&cin, g);
	else
	{
		ifstream *fl = new ifstream(argv[1], ios_base::in);
		parsePace(fl, g);
		delete fl;
	}

	auto n = num_vertices(g);
	std::vector<int> colorV(n,-1);
	
	graph_traits<Graph>::edge_iterator ei, eend;
	graph_traits<Graph>::vertex_iterator vi, viend;
	for(tie(ei, eend) = edges(g) ; ei != eend; ++ei)
	{
		auto e = *ei;
		int u = source(e, g);
		int v = target(e,g);
		printf("Edge %d -- %d\n", u,v);
	}
	typedef boost::property_map< Graph, boost::vertex_index_t >::type
			VertexIndexMap;
	VertexIndexMap vertex_id = boost::get(boost::vertex_index, g);
	typedef boost::iterator_property_map< std::vector<int>::iterator, VertexIndexMap, int, int& >
			VecIterMap;
	
	VecIterMap color_pav(colorV.begin(), vertex_id);

	greedy_coloring(g, color_pav);
	for(tie(vi, viend) = vertices(g) ; vi != viend ; ++vi)
	{
		auto v = *vi;
		int cv = get(color_pav, v);
		printf("Vertex %lu, color %d\n", v, cv);
	}

	return 0;
}
