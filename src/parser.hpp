#ifndef PARSER_HPP
#define PARSER_HPP
#include <istream>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace boost;
using namespace std;

template <class VertexListGraph>
void parsePace(istream *gstream, VertexListGraph &g)
{
	int numVertices = 0;
	int numEdges = 0;
	std::string line;
	while(!gstream->eof())
	{
		getline(*gstream, line);
		istringstream lineStream(line);
		if(line.length() == 0)
			continue;
		if(line[0] == 'c') // comment
			continue;
		switch(line[0])
		{
			case 'c': // comment
				continue;
			case 'p':
			{
				string s;
				lineStream >> s >> s >> numVertices >> numEdges;
				break;
			}
			default:
			{
				int v,u;
				lineStream >> v >> u;
				//std::cout << "Edge " << v << "," << u << "\n";
				add_edge(u-1,v-1,g);
			}
		}
	}

}

#endif
