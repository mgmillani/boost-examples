CC = g++
CFLAGS = -std=c++11 -Wall -Wextra
SRC = src
OBJ = obj
BIN = bin

all: bin/coloring

$(OBJ):
	mkdir -p $(OBJ)
$(BIN):
	mkdir -p $(BIN)

release: CFLAGS += -s -O3 -D TRACE_OFF
release: $(BIN)/$(NAME)

debug: CFLAGS += -g -O0
debug: $(BIN)/$(NAME)

bin/coloring: src/coloring.cpp src/parser.hpp
	$(CC) $(CFLAGS) src/coloring.cpp -o $@

clean:
	rm -f bin/coloring
